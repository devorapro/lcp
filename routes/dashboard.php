<?php

Route::get('/', 'Dashboard')->name('DashboardPage');
Route::get('/mail', 'Mail')->name('MailSettingsPage');
Route::get('/basic', 'Basic')->name('BasicSettingsPage');
Route::get('/db', 'DB')->name('DatabaseSettingsPage');

Route::post('/mail', 'Mail@change');
Route::post('/basic', 'Basic@change');
Route::post('/db', 'DB@change');
//Route::post('/db/sqlite', 'DB@sqlite_change')->name('SQLiteSettingsChange');
//Route::post('/db/mysql', 'DB@mysql_change')->name('MysqlSettingsChange');
//Route::post('/db/pgsql', 'DB@pgsql_change')->name('PGSqlSettingsChange');
//Route::post('/db/sqlsrv', 'DB@sqlsrv_change')->name('SqlsrvSettingsChange');
<?php

Route::get('/', 'Auth')->name('AuthPage');
Route::get('/join', 'Join')->name('JoinPage');
if (config('settings.registration.verification')) {
	Route::get('/join/activate', 'Activate')->name('ActivatePage');
	Route::post('/join/activate', 'Activate@resendMail');
	Route::get('/join/activate/{code}', 'Activate@activate');
}

Route::post('/', 'Auth@authMe');
Route::post('/join', 'Join@reg');
Route::post('/logout', 'Auth@logout')->name('logout');

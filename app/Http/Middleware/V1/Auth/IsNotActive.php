<?php

namespace App\Http\Middleware\V1\Auth;

use Closure;

class IsNotActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if ($request->user()->status != 0) {
            return redirect(route('ControlPanelPage'));
        }
        
        return $next($request);

    }
}

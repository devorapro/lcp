<?php

namespace App\Http\Middleware\V1\Auth;

use Closure;

class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if ($request->user()->status != 1) {
            return redirect(route('ActivatePage'));
        }
        
        return $next($request);

    }
}

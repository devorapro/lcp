<?php

namespace App\Http\Middleware\V1\Auth;

use Closure;
use Auth;

class CheckGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }

        return redirect(route('ControlPanelPage'));
    }
}

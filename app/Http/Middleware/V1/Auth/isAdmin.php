<?php

namespace App\Http\Middleware\V1\Auth;

use Auth;
use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || !$request->user()->is_admin) {
            abort(404);
        }
        
        return $next($request);
    }
}

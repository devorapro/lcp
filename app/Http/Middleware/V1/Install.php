<?php

namespace App\Http\Middleware\V1;

use Closure;

class Install
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!env('APP_INSTALLED')) {
            if ($_SERVER['REQUEST_URI'] != '/install') {
                return redirect(route('InstallPage'));
            } else {
                return $next($request);
            }
        }
        return $next($request);
    }
}

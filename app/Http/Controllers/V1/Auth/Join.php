<?php

namespace App\Http\Controllers\V1\Auth;

use Auth;
use App\Models\V1\User;
use Illuminate\Http\Request;
use App\Mail\V1\Auth\Verify;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\V1\Auth\JoinRequest;

class Join extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function __invoke()
    {
    	return view('pages.auth.JoinPage');
    }

    public function reg(JoinRequest $req)
    {
    	if (config('settings.registration.verification')) {
    		$status = 0;
    		$verify_code = bin2hex(random_bytes(42));
            Mail::to($req->email)->send(new Verify($verify_code));
    		$message = 'На ваш почтовый ящик было выслано письмо с дальнейшими инструкциями по подтверждению аккаунта';
    	} else {
    		$status = 1;
    		$verify_code = null;
    		$message = 'Теперь вы можете войти в личный кабинет';
    	}

        $user = new User();
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->status = $status;
        $user->verify_code = $verify_code;
        $user->save();

        Auth::login($user);

        return redirect(route('ControlPanelPage'));
    }
}

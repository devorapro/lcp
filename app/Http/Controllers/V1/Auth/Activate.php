<?php

namespace App\Http\Controllers\V1\Auth;

use App\Models\V1\User;
use Illuminate\Http\Request;
use App\Mail\V1\Auth\Verify;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class Activate extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('is_not_active');
	}

    public function __invoke()
    {
    	return view('pages.auth.ActivatePage');
    }

    public function resendMail(Request $req)
    {
    	Mail::to($req->user()->email)->send(new Verify($req->user()->verify_code));
    }

    public function activate($code)
    {

    	$user = User::where('verify_code', $code)->first();

        if (!$user) { 
    	   return redirect(route('AuthPage'));
        }

        $user->verify_code = null;
        $user->status = 1;
        $user->save();

        return redirect(route('AuthPage'));
    }
}

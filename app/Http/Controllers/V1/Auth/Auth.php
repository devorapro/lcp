<?php

namespace App\Http\Controllers\V1\Auth;

use Session;
use Auth as AuthMe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Auth extends Controller
{
	public function __construct()
	{
		$this->middleware('guest')
             ->except('logout');
	}

    public function __invoke()
    {
    	return view('pages.auth.AuthPage');
    }

    public function authMe(Request $req)
    {
        if (AuthMe::attempt(['email' => $req->email, 'password' => $req->password])) {
          return redirect(route('ControlPanelPage'));
        }

        Session::flash('authFail', 'Не верный логин или пароль');
        return redirect(route('AuthPage'));
    }

    public function logout(Request $req)
    {
    	AuthMe::logout();
    	return redirect(route('AuthPage'));
    }
}

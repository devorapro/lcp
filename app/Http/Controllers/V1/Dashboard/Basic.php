<?php

namespace App\Http\Controllers\V1\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\V1\Settings;
use Session;

class Basic extends Controller
{
    public function __construct()
    {
    	$this->middleware('is_admin');
    }

    public function __invoke()
    {
    	return view('pages.dashboard.BasicSettingsPage');
    }

    public function change(Request $req)
    {
    	Settings::set('APP_LOGO', $req->app_name);

    	Session::flash('MailSettingsChanged', 'Настройки успешно сохранены');

    	return redirect(route('BasicSettingsPage'));
    }
}

<?php

namespace App\Http\Controllers\V1\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\V1\Settings;
use Session;

class DB extends Controller
{
    public function __construct()
    {
    	$this->middleware('is_admin');
    }

    public function __invoke()
    {
    	return view('pages.dashboard.DatabaseSettingsPage');
    }

    public function change(Request $req)
    {
        Settings::set('DB_CONNECTION', $req->database_connection);
        Settings::set('DB_HOST', $req->db_host);
        Settings::set('DB_PORT', $req->db_port);
        Settings::set('DB_DATABASE', $req->db_database);
        Settings::set('DB_USERNAME', $req->db_username);
    	Settings::set('DB_PASSWORD', $req->db_password);

    	Session::flash('DBSettingsChanged', 'Настройки успешно сохранены');

    	return redirect(route('DatabaseSettingsPage'));
    }
}

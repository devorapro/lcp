<?php

namespace App\Http\Controllers\V1\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\V1\Settings;
use Session;

class Mail extends Controller
{
    public function __construct()
    {
    	$this->middleware('is_admin');
    }

    public function __invoke()
    {
    	return view('pages.dashboard.MailSettingsPage');
    }

    public function change(Request $req)
    {
    	if (!empty($req->mail_password)) {
            Settings::set('MAIL_PASSWORD', $req->mail_password);
    	}

        Settings::set('MAIL_DRIVER', $req->mail_driver);
        Settings::set('MAIL_HOST', $req->mail_host);
        Settings::set('MAIL_PORT', $req->mail_port);
        Settings::set('MAIL_ENCRYPTION', $req->mail_encryption);
        Settings::set('MAIL_USERNAME', $req->mail_username);
        Settings::set('MAIL_FROM_EMAIL', $req->mail_username);
        Settings::set('MAIL_FROM_NAME', str_replace(' ', '_', $req->mail_name));

    	Session::flash('MailSettingsChanged', 'Настройки успешно сохранены');

    	return redirect(route('MailSettingsPage'));
    }
}

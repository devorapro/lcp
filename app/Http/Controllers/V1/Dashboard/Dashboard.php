<?php

namespace App\Http\Controllers\V1\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dashboard extends Controller
{
    public function __construct()
    {
    	$this->middleware('is_admin');
    }

    public function __invoke()
    {
    	return view('pages.dashboard.DashboardPage');
    }
}

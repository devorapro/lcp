<?php

namespace App\Http\Controllers\V1\CP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Index extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('is_active');
	}

    public function __invoke(Request $req)
    {
    	return view('pages.cp.index');
    }
}

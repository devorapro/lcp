<?php

namespace App\Http\Requests\V1\Auth;

use Illuminate\Foundation\Http\FormRequest;

class JoinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:255|unique:users,name',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:8|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'name.min' => 'Значение поля "Никнейм" не может быть короче 4 символов',
            'name.max' => 'Значение поля "Никнейм" не может быть длиннее 255 символов',
            'name.required' => 'Поле "Никнейм" должно быть заполнено',
            'name.unique' => 'Пользователь с таким именем уже существует',

            'email.email' => 'Введите корректный почтовый адрес',
            'email.max' => 'Поле "Почтовый адрес" не может быть длиннее 255 символов',
            'email.required' => 'Поле "Почтовый адрес" должно быть заполнено',
            'email.unique' => 'Пользоватлеь с таким почтовым адресом уже существует',

            'password.min' => 'Значение поля "Пароль" не может быть короче 8 символов',
            'password.required' => 'Поле "Пароль" должно быть заполнено',
            'password.confirmed' => 'Пароли не совпадают',
        ];
    }
}

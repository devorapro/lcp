<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $install_namespace = 'App\Http\Controllers\V1\Install';
    protected $auth_namespace = 'App\Http\Controllers\V1\Auth';
    protected $cp_namespace = 'App\Http\Controllers\V1\CP';
    protected $dashboard_namespace = 'App\Http\Controllers\V1\Dashboard';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        //$this->mapApiRoutes();

        if (!env('APP_INSTALLED')) {
            $this->mapInstallRoutes();
        }
        $this->mapAuthRoutes();
        $this->mapCPRoutes();
        $this->mapDashboardRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapInstallRoutes()
    {
        Route::middleware('web')
             ->namespace($this->install_namespace)
             ->group(base_path('routes/install.php'));
    }

    protected function mapAuthRoutes()
    {
        Route::middleware('web')
             ->namespace($this->auth_namespace)
             ->group(base_path('routes/auth.php'));
    }

    protected function mapCPRoutes()
    {
        Route::prefix('cp')
             ->middleware('web')
             ->namespace($this->cp_namespace)
             ->group(base_path('routes/cp.php'));
    }

    protected function mapDashboardRoutes()
    {
        Route::prefix('dashboard')
             ->middleware('web')
             ->namespace($this->dashboard_namespace)
             ->group(base_path('routes/dashboard.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}

<?php

return [
	'app' => [
		'logo' => env('APP_LOGO', 'Logotype')
	],
	'https' => false,
	'url' => 'http://localhost',
    'logo' => 'Logotype',
    'registration' => [
    	'verification' => true,
    	'subject' => 'Активация аккаунта'
    ]

];

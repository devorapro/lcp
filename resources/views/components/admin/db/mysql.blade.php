<div class="mt-3 mb-3">
	<strong style="font-size: 1.2rem;">
		Mysql
	</strong>
	<small class="text-muted">Мы рекомендуем <strong>Mysql 5.7</strong>. Стабильная работа на более ранних версиях не гарантирована.</small>
</div>
<form action="{{ route('MysqlSettingsChange') }}" method="post">
	@csrf
	<div class="form-group">
		<label>Сервер базы данных</label>
		<input type="text" class="form-control" placeholder="Database host" name="db_host" value="{{config('database.connections.mysql.host')}}">
		<div class="text-muted setting-description mt-3"></div>
	</div>
	<div class="form-group">
		<label>Порт базы данных</label>
		<input type="text" class="form-control" placeholder="Database port" name="db_port" value="{{config('database.connections.mysql.port')}}">
		<div class="text-muted setting-description mt-3"></div>
	</div>
	<div class="form-group">
		<label>Имя базы данных</label>
		<input type="text" class="form-control" placeholder="Database name" name="db_database" value="{{config('database.connections.mysql.database')}}">
		<div class="text-muted setting-description mt-3"></div>
	</div>
	<div class="form-group">
		<label>Имя пользователя</label>
		<input type="text" class="form-control" placeholder="Database username" name="db_username" value="{{config('database.connections.mysql.username')}}">
		<div class="text-muted setting-description mt-3"></div>
	</div>
	<div class="form-group">
		<label>Пароль</label>
		<input type="text" class="form-control" placeholder="Database password" name="db_password">
		<div class="text-muted setting-description mt-3"></div>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary">Отправить</button>
	</div>
</form>
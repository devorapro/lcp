<div class="col-3" style="border-right: 1px solid rgba(0,0,0,.1); padding-right: 0;">
	<div class="pb-3 pt-3">
		<nav class="nav flex-column cp-nav">
	        <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="{{route('DashboardPage')}}"><i class="fas fa-cog"></i>&nbsp;Панель управления</a>
	        <a class="nav-link {{ Request::is('dashboard/basic') ? 'active' : '' }}" href="{{route('BasicSettingsPage')}}"><i class="fas fa-sliders-h"></i>&nbsp;Основные</a>
	        <a class="nav-link {{ Request::is('dashboard/mail') ? 'active' : '' }}" href="{{route('MailSettingsPage')}}"><i class="fas fa-at"></i>&nbsp;Почта</a>
	        <a class="nav-link {{ Request::is('dashboard/db') ? 'active' : '' }}" href="{{route('DatabaseSettingsPage')}}"><i class="fas fa-database"></i>&nbsp;База данных</a>
		</nav>
	</div>
</div>
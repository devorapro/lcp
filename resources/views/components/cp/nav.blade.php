<nav class="nav auth-nav">
	@if (Auth::check() && Auth::user()->is_admin)
	<a href="{{route('DashboardPage')}}" class="nav-link">Администратор</a>
	@endif
	<form method="post" action="{{route('logout')}}">
		@csrf
		<button class="btn nav-link btn-link">Выйти</button>
	</form>
</nav>
@section('head')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<title>Dev</title>
</head>
<body>
@show
{{-- <div class="ui-loading-wrapper d-flex justify-content-center align-items-center">
	<div class="db-spinner"></div>
</div> --}}
<div class="auth-header pt-3 pb-3">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-6">
				@include('components.Logo')
			</div>
			<div class="col-6 d-flex justify-content-end">
				@include('components.auth.nav')
			</div>
		</div>
	</div>
</div>
@yield('content')
@section('footer')
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
{{-- <style type="text/css">
    .ui-loading-wrapper {
    	background-color: rgba(0,0,0,.8);
    	position: absolute;
    	top: 0;
    	left: -100%;
    	width: 100vw;
    	height: 100vh;
    	z-index: 99999;
    }
	.db-spinner {
	    width: 75px;
	    height: 75px;
	    border-radius: 50%;
	    background-color: transparent;
	    border: 2px solid #222;
	    border-top: 2px solid #03A9F4;
	    border-bottom: 2px solid #03A9F4;
	    -webkit-animation: 1s spin linear infinite;
	    animation: 1s spin linear infinite;
	}

	@-webkit-keyframes spin {
	    from {
	        -webkit-transform: rotate(0deg);
	        transform: rotate(0deg);
	    }
	    to {
	        -webkit-transform: rotate(360deg);
	        transform: rotate(360deg);
	    }
	}
	          
	@keyframes spin {
	    from {
	        -webkit-transform: rotate(0deg);
	        transform: rotate(0deg);
	    }
	    to {
	        -webkit-transform: rotate(360deg);
	        transform: rotate(360deg);
	    }
	}    
</style> --}}
</body>
</html>
@show
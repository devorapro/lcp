@section('head')
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<title>Dev</title>
</head>
<body>
@show
<div class="auth-header pt-3 pb-3">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-6">
				@include('components.Logo')
			</div>
		</div>
	</div>
</div>
@yield('content')
@section('footer')
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>
@show
<div>
	<p>
		Вас приветствует {{ config('settings.logo') }}
	</p>
	<p>
		Для активации аккаунта <a href="{{ config('settings.url') }}/join/activate/{{ $code }}">подтвердите</a> регистрацию.
	</p>
</div>
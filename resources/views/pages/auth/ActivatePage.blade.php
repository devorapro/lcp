@extends('layouts.CPLayout')
@section('content')
	<div class="text-center mt-3">
		<h2 class="mb-4">
			Проверьте свой почтовый ящик
		</h2>
		<p>
			Мы отправили электронное письмо по адресу <strong>{{Auth::user()->email}}</strong>.<br>
			Следуйте инструкциям, чтобы подтвердить свой аккаунт
		</p>
		<img width="170" height="170" class="mb-4" src="{{asset('img/email-verification-img.svg')}}">
		<p>
			<small>
				Не получили письмо? <a id="resendMail" href="javascript:void(0);">Отправить еще раз</a>
			</small>
		</p>
	</div>
@endsection
@extends('layouts.AuthLayout')
@section('content')
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-6">
			<div class="card" style="border-radius: 0;">
			  <div class="card-header">
			    <div class="row d-flex align-items-center">
			    	<div class="col-6">
			    		<strong>Войти</strong>
			    	</div>
			    	<div class="col-6 d-flex justify-content-end">
			    		<a href="#">Не помню пароль</a>
			    	</div>
			    </div>
			  </div>
			  <div class="card-body">
			    <form method="post">
			    	@csrf
			    	<div class="form-group">
			    		<label for="email">Почтовый адрес</label>
					    <input type="email" class="form-control" placeholder="mail@example.com" name="email">
			    	</div>
			    	<div class="form-group">
			    		<label for="password">Пароль</label>
					    <input type="password" class="form-control" placeholder="Password" name="password">
			    	</div>
			    	@if (Session::get('authFail'))
			    	<div class="form-group">
			    		<div class="alert alert-danger" role="alert">
						    <strong>{{ Session::get('authFail') }}</strong>
						</div>
			    	</div>
			    	@endif
			    	<div class="form-group">
			    		<button id="authMe" type="submit" class="btn btn-block btn-primary">Войти</button>
			    	</div>
			    </form>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection
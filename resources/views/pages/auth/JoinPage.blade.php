@extends('layouts.AuthLayout')
@section('content')
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-6">
			<div class="card" style="border-radius: 0;">
			  <div class="card-header">
			    <strong>Регистрация</strong>
			  </div>
			  <div class="card-body">
			    <form method="post">
			    	@csrf
			    	<div class="form-group">
			    		<label for="name">Никнейм</label>
			    		@if (!$errors->has('name'))
					    <input name="name" type="text" class="form-control" placeholder="Nickname" id="name">
			    		@else
					    <input name="name" type="text" class="form-control is-invalid" placeholder="Nickname" id="name" value="{{ old('name') }}">
					    <small class="text-danger">{{ $errors->first('name') }}</small>
			    		@endif
			    	</div>
			    	<div class="form-group">
			    		<label for="email">Почтовый адрес</label>
					    @if (!$errors->has('email'))
					    <input name="email" type="email" class="form-control" placeholder="mail@example.com" id="email">
			    		@else
					    <input name="email" type="email" class="form-control is-invalid" placeholder="mail@example.com" id="email" value="{{ old('email') }}">
					    <small class="text-danger">{{ $errors->first('email') }}</small>
			    		@endif
			    	</div>
			    	<div class="form-group">
			    		<div class="row">
			    			<div class="col-6">
			    				<label for="password">Пароль</label>
			    				@if (!$errors->has('password'))
					    		<input name="password" type="password" class="form-control" placeholder="Password" id="password">
					    		@else
					    		<input name="password" type="password" class="form-control is-invalid" placeholder="Password" id="password">
					    		<small class="text-danger">{{ $errors->first('password') }}</small>
					    		@endif
			    			</div>
			    			<div class="col-6">
			    				<label for="password_confirmation">Подтвердите пароль</label>
			    				@if ($errors->has('password') && $errors->first('password') == 'Пароли не совпадают')
					    		<input name="password_confirmation" type="password" class="form-control is-invalid" placeholder="Password" id="password_confirmation">
					    		@else
					    		<input name="password_confirmation" type="password" class="form-control" placeholder="Password" id="password_confirmation">
					    		@endif
			    			</div>
			    		</div>
			    	</div>
			    	<div class="form-group">
			    		<button type="submit" class="btn btn-block btn-primary">Войти</button>
			    	</div>
			    </form>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection
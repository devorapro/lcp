@extends('layouts.CPLayout')
@section('content')
<div class="container mt-3">
	<div class="card" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
		<div class="row">
			@include('components.cp.SideBar')
			<div class="col-9 p-3">
				Content
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.cp-nav > .nav-link {
		padding-top: 10px;
		padding-bottom: 10px;
		color: #000;
	}
	.cp-nav > .nav-link:hover {
		color: #1890ff;
	}
	.cp-nav > .active {
		color: #1890ff;
		background-color: #e6f7ff;
		border-right: 3px solid #1890ff;
	}
</style>
@endsection
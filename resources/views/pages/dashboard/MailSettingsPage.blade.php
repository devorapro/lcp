@extends('layouts.CPLayout')
@section('content')
<div class="container mt-3">
	<div class="card" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
		<div class="row">
			@include('components.admin.SideBar')
			<div class="col-9">
				<div class="card-body">
					<strong style="font-size: 1.5rem;">
						Почта
					</strong>
					@if (Session::get('MailSettingsChanged'))
					<div class="alert alert-success" role="alert">
					  {{Session::get('MailSettingsChanged')}}
					</div>
					@endif
					<div class="alert alert-light" role="alert">
					  Здесь можно изменить параметры отправки сообщений на почту
					</div>
					<form method="post">
						@csrf
				    	<div class="form-group">
				    		<label for="mail_driver">Драйвер почты</label>
						    <input type="text" class="form-control" placeholder="Mail driver" name="mail_driver" value="{{config('mail.driver')}}">
						    <div class="text-muted setting-description mt-3">
						    	Драйвер почты - обязательное поле. Вам всегда доступен драйвер <strong>mail</strong>.<br/> Предпочтительно использовать драйвер <strong>smtp</strong>, при условии что ваш хостинг провайдер предоставляет возможность отправки писем по данному протоколу. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_host">Почтовый сервер</label>
						    <input type="text" class="form-control" placeholder="Mail host" name="mail_host" value="{{config('mail.host')}}">
						    <div class="text-muted setting-description mt-3">
						    	Почтовый сервер - обязательное поле, если вы используете <strong>драйвер smtp</strong>.<br/>
						    	Подробности уточняйте у хостинг провайдера. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_port">Порт сервера</label>
						    <input type="text" class="form-control" placeholder="Mail port" name="mail_port" value="{{config('mail.port')}}">
						    <div class="text-muted setting-description mt-3">
						    	Порт сервера - обязательное поле, если вы используете <strong>драйвер smtp</strong>.<br/>
						    	Подробности уточняйте у хостинг провайдера. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_name">Имя отправителя</label>
						    <input type="text" class="form-control" placeholder="Mail name" name="mail_name" value="{{str_replace('_', ' ', config('mail.from.name'))}}">
						    <div class="text-muted setting-description mt-3">
						    	Имя отправителя - не обязательно поле. Заполните его, чтобы ваши уведомления приходили от данного имени. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_encryption">Протокол защиты</label>
						    <input type="text" class="form-control" placeholder="Mail encryption" name="mail_encryption" value="{{config('mail.encryption')}}">
						    <div class="text-muted setting-description mt-3">
						    	Протокол защиты - не обязательное поле. Работает только с <strong>smtp</strong>.<br/>
						    	Подробности уточняйте у хостинг провайдера. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_username">Почтовый ящик</label>
						    <input type="text" class="form-control" placeholder="Mail login" name="mail_username" value="{{config('mail.username')}}">
						    <div class="text-muted setting-description mt-3">
						    	Почтовый ящик - обязательное поле, если вы используете <strong>драйвер smtp</strong>. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<label for="mail_password">Пароль</label>
						    <input type="password" class="form-control" placeholder="Mail password" name="mail_password">
						    <div class="text-muted setting-description mt-3">
						    	Пароль от вашего почтового ящика. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<button type="submit" class="btn btn-primary">Отправить</button>
				    	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
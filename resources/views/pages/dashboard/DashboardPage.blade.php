@extends('layouts.CPLayout')
@section('content')
<div class="container mt-3">
	<div class="card" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
		<div class="row">
			@include('components.admin.SideBar')
			<div class="col-9">
				<div class="card-body">
					<strong style="font-size: 1.5rem;">
						Панель управления
					</strong>
					<div class="alert alert-light" role="alert">
					  Это панель управления. Здесь можно поменять настройки личного кабинета, а так же просмотреть статистику.<br>
					  Ниже расположена инструкция к применению:
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.cp-nav > .nav-link {
		padding-top: 10px;
		padding-bottom: 10px;
		color: #000;
	}
	.cp-nav > .nav-link:hover {
		color: #1890ff;
	}
	.cp-nav > .active {
		color: #1890ff;
		background-color: #e6f7ff;
		border-right: 3px solid #1890ff;
	}
</style>
@endsection
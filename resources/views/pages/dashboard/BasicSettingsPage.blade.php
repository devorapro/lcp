@extends('layouts.CPLayout')
@section('content')
<div class="container mt-3">
	<div class="card" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
		<div class="row">
			@include('components.admin.SideBar')
			<div class="col-9">
				<div class="card-body">
					<strong style="font-size: 1.5rem;">
						Основные настройки
					</strong>
					@if (Session::get('MailSettingsChanged'))
					<div class="alert alert-success" role="alert">
					  {{Session::get('MailSettingsChanged')}}
					</div>
					@endif
					<div class="alert alert-light" role="alert">
					  Здесь можно изменить основные параметры личного кабинета
					</div>
					<form method="post">
						@csrf
				    	<div class="form-group">
				    		<label for="mail_driver">Название проекта</label>
						    <input type="text" class="form-control" placeholder="Application name" name="app_name" value="{{config('settings.app.logo')}}">
						    <div class="text-muted setting-description mt-3">
						    	Название проекта - обязательно поле. Название вашего проета будет использоваться в качестве логотипа. 
						    </div>
				    	</div>
				    	<div class="form-group">
				    		<button type="submit" class="btn btn-primary">Отправить</button>
				    	</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@extends('layouts.CPLayout')
@section('content')
<div class="container mt-3">
	<div class="card" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
		<div class="row">
			@include('components.admin.SideBar')
			<div class="col-9">
				<div class="card-body">
					<strong style="font-size: 1.5rem;">
						База данных личного кабинета
					</strong>
					@if (Session::get('DBSettingsChanged'))
					<div class="alert alert-success" role="alert">
					  {{Session::get('DBSettingsChanged')}}
					</div>
					@endif
					<div class="alert alert-light" role="alert">
					  Здесь можно изменить параметры базы данных личного кабинета<br/>
					  Личный кабинет работает с <strong>SQLite</strong>, <strong>Mysql</strong>, <strong>PostgreSQL</strong> и <strong>Microsoft SQL Server</strong>
					</div>
					{{-- <ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link {{ config('database.default') == 'sqlite' ? 'active' : '' }}" id="home-tab" data-toggle="tab" href="#sqlite" role="tab" aria-controls="sqlite" aria-selected="true">SQLite</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link {{ config('database.default') == 'mysql' ? 'active' : '' }}" id="profile-tab" data-toggle="tab" href="#mysql" role="tab" aria-controls="mysql" aria-selected="false">Mysql</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link {{ config('database.default') == 'pgsql' ? 'active' : '' }}" id="contact-tab" data-toggle="tab" href="#pgsql" role="tab" aria-controls="pgsql" aria-selected="false">PostgreSQL</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link {{ config('database.default') == 'sqlsrv' ? 'active' : '' }}" id="contact-tab" data-toggle="tab" href="#sqlsrv" role="tab" aria-controls="sqlsrv" aria-selected="false">SQL Server</a>
					  </li>
					</ul>
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade {{ config('database.default') == 'sqlite' ? 'show active' : '' }}" id="sqlite" role="tabpanel" aria-labelledby="sqlite-tab">@include('components.admin.db.sqlite')</div>
					  <div class="tab-pane fade {{ config('database.default') == 'mysql' ? 'show active' : '' }}" id="mysql" role="tabpanel" aria-labelledby="mysql-tab">@include('components.admin.db.mysql')</div>
					  <div class="tab-pane fade {{ config('database.default') == 'pgsql' ? 'show active' : '' }}" id="pgsql" role="tabpanel" aria-labelledby="pgsql-tab">@include('components.admin.db.pgsql')</div>
					  <div class="tab-pane fade {{ config('database.default') == 'sqlsrv' ? 'show active' : '' }}" id="sqlsrv" role="tabpanel" aria-labelledby="sqlsrv-tab">@include('components.admin.db.sqlsrv')</div>
					</div> --}}
					<form method="post">
						@csrf
				    	<div class="form-group">
				    		<label for="database_connection">Драйвер базы данных</label>
						    <select name="database_connection" class="custom-select">
							  <option value="sqlite" {{ config('database.default') == 'sqlite' ? 'selected' : '' }} >SQLite</option>
							  <option value="mysql" {{ config('database.default') == 'mysql' ? 'selected' : '' }} >Mysql</option>
							  <option value="pgsql" {{ config('database.default') == 'pgsql' ? 'selected' : '' }} >PostgreSQL</option>
							  <option value="sqlsrv" {{ config('database.default') == 'sqlsrv' ? 'selected' : '' }} >SQL Server</option>
							</select>
				    	</div>
						<div class="form-group">
							<label>Сервер базы данных</label>
							<input type="text" class="form-control" placeholder="Database host" name="db_host" value="{{config('database.connections.mysql.host')}}">
							<div class="text-muted setting-description mt-3"></div>
						</div>
						<div class="form-group">
							<label>Порт базы данных</label>
							<input type="text" class="form-control" placeholder="Database port" name="db_port" value="{{config('database.connections.mysql.port')}}">
							<div class="text-muted setting-description mt-3"></div>
						</div>
						<div class="form-group">
							<label>Имя базы данных</label>
							<input type="text" class="form-control" placeholder="Database name" name="db_database" value="{{config('database.connections.mysql.database')}}">
							<div class="text-muted setting-description mt-3"></div>
						</div>
						<div class="form-group">
							<label>Имя пользователя</label>
							<input type="text" class="form-control" placeholder="Database username" name="db_username" value="{{config('database.connections.mysql.username')}}">
							<div class="text-muted setting-description mt-3"></div>
						</div>
						<div class="form-group">
							<label>Пароль</label>
							<input type="text" class="form-control" placeholder="Database password" name="db_password">
							<div class="text-muted setting-description mt-3"></div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Отправить</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
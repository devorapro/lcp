@extends('layouts.InstallLayout')
@section('content')
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-md-8 col-sm-12">
			<div class="card shadow-sm" style="border-radius: 0; border: 1px solid rgba(0,0,0,.1);">
				<div class="card-header bg-light">
					<strong>Установка</strong>
				</div>
				<div class="card-body">
					<h5 class="card-title">Требования к серверу</h5>
					<div>
						Версия PHP: @if (version_compare(PHP_VERSION, '7.1.3') > 0)<strong class="text-success">{{ phpversion() }}</strong>@else<strong class="text-danger">{{ phpversion() }}</strong>@endif
					</div>
					<table class="table table-borderless">
					  <thead>
					    <tr>
					      <th scope="col">Расширение</th>
					      <th scope="col">Загружено сервером</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>OpenSSL</td>
					      <td>
					      	@if (extension_loaded('openssl'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>PDO</td>
					      <td>
					      	@if (extension_loaded('PDO'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>Mbstring</td>
					      <td>
					      	@if (extension_loaded('mbstring'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>Tokenizer</td>
					      <td>
					      	@if (extension_loaded('tokenizer'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>Ctype</td>
					      <td>
					      	@if (extension_loaded('ctype'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>XML</td>
					      <td>
					      	@if (extension_loaded('xml'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					    <tr>
					      <td>JSON</td>
					      <td>
					      	@if (extension_loaded('json'))<strong class="text-success">Да</strong>@else<strong class="text-danger">Нет</strong>@endif
					      </td>
					    </tr>
					  </tbody>
					</table>
					<hr>
					<h5 class="card-title">База данных</h5>
					<div class="form-group">
						<label>Сервер</label>
						<input type="text" name="db_host" value="localhost" class="form-control">
					</div>
					<div class="form-group">
						<label>База данных</label>
						<input type="text" name="db_database" class="form-control">
					</div>
					<div class="form-group">
						<label>Имя пользователя</label>
						<input type="text" name="db_username" class="form-control">
					</div>
					<div class="form-group">
						<label>Пароль</label>
						<input type="password" name="db_password" class="form-control">
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
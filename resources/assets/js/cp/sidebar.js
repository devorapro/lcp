import $ from 'jquery';

let navLinks = $('.nav-link');

for (var i = navLinks.length - 1; i >= 0; i--) {
	if (navLinks[i].hasAttribute('path')) {
		if (navLinks[i].getAttribute('path') === window.location.pathname) {
			navLinks[i].classList.add('active');
		}
	}
}
import axios from 'axios';

resendMail.onclick = () => {
	axios.post('activate')
	.then(onSuccess).catch(onError);
}

function onSuccess(res) {
	new Noty({
		theme: 'mint',
    	type: 'success',
	    layout: 'bottomRight',
	    text: 'Письмо с активацией успешно отправлено'
	}).show().setTimeout(4500);
}

function onError(err) {
	console.log(err.response);
}
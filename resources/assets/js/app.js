require('./bootstrap');
window.Noty = require('noty');

switch(window.location.pathname) {
  case '/':
    require('./auth/AuthPage');
    break;
  case '/join':
    require('./auth/JoinPage');
    break;
  case '/join/activate':
    require('./auth/ActivatePage');
    break;
  case '/cp':
    require('./cp/index');
    break;
}